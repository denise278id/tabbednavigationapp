﻿using System.Diagnostics;
using Xamarin.Forms;

namespace TabbedNavigationApp
{
    public partial class TabbedNavigationAppPage : TabbedPage
    {
        public TabbedNavigationAppPage()
        {
            InitializeComponent();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(TabbedNavigationAppPage)}:  ctor");
        }

        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");
        }

        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
        }

    }
}
