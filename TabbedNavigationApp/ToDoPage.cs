﻿sing System;

using Xamarin.Forms;

namespace TabbedNavigationApp
{
    public class ToDoPage : ContentPage
    {
        public ToDoPage()
        {
            Content = new StackLayout
            {
                Children = {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}

